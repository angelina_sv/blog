async function deleteP(url) {   
    const result = await fetch(url, {
        method: 'DELETE',
        credentials: 'same-origin'
    });
    
    if(result.status === 200) {
        window.location.href = '/publications';
    } else {
        const panel = document.createElement('p');
        panel.innerHTML = 'Error';
        document.body.appendChild(panel);

    }
}

async function goToEditP(url) {
    window.location.href = url;
}

async function editP(url) {
    const publicationId = url.split('/').reverse()[0];
    const name = document.getElementById('name').textContent;
    const text = document.getElementById('text').textContent;
    const picture = document.getElementById('editPicture');

    const fData = new FormData();
    fData.append('name', name);
    fData.append('text', text);

    if(picture.files[0]) {
        fData.append('images', picture.files[0], 'test.png');
    }
   
    const result = await fetch(url, {
        method: 'POST',
        credentials: 'same-origin',
        body: fData
    });

    if(result.status === 200) {
        window.location.href = `/publications/${publicationId}`;
    }
}