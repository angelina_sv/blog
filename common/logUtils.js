const util = require('util');

function consoleDebug(obj) {
    console.log(util.inspect(obj, { depth: null, compact: true, colors: true }));
}

module.exports = {
    consoleDebug,
}