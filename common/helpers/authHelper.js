const crypto = require('crypto');
const jwt  = require('jsonwebtoken');

function hashPassword(password) {
    const hashedPassword = crypto.createHmac('sha256', 'You shall not pass')
                            .update(password)
                            .digest('hex');

    return hashedPassword;
}

function generateToken(payload) {
  return new Promise((res, rej) => {
    jwt.sign({
      ...payload, 
    }, 
    process.env.TOKEN_PRIVATE_KEY, 
    {expiresIn: 5 * 60 * 60},
    (err, encoded) => {
      if (err) {
        return rej(err);
      }
      res(encoded);
    });
  });  
}

function verifyToken(token) {
    return new Promise((res, rej) => {
        jwt.verify(
            token, 
            process.env.TOKEN_PRIVATE_KEY,
            (err, decoded) => {
                if (err) {
                    return rej(err);
                }
                res(decoded);
            }
        )
    });
}

module.exports = {
    hashPassword,
    generateToken,
    verifyToken,
};

