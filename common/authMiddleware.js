const { verifyToken } = require('./helpers/authHelper');

module.exports = async function ensureAuthorized(req, res, next) {
    if(req.originalUrl === '/publications/list' && req.method === 'GET') {
        return next();
    }

    const token = req.cookies.token;

    try {
        const decoded = await verifyToken(token);
        req.user = decoded;
    } catch(e) {
        return res.status(401).json({ message: 'Invalid token' });
    }

    next();
};
