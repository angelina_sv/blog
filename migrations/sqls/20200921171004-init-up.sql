CREATE TABLE public.users
(
    id serial NOT NULL,
    nickname character varying(128) NOT NULL,
    email character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    created_at timestamp(0) with time zone DEFAULT now(),
    CONSTRAINT users_pkey PRIMARY KEY (id),
    CONSTRAINT users_email_key UNIQUE (email),
    CONSTRAINT users_nickname_key UNIQUE (nickname)
);

ALTER TABLE public.users
    OWNER to postgres;


CREATE TABLE public.articles
(
    id serial NOT NULL,
    author_id integer NOT NULL,
    title character varying(255) NOT NULL,
    "text" text NOT NULL,
    picture_path text,
    created_at timestamp(0) with time zone DEFAULT now(),
    CONSTRAINT articles_pkey PRIMARY KEY (id),
    CONSTRAINT fk_author FOREIGN KEY(author_id) REFERENCES users(id)
);

ALTER TABLE public.articles
    OWNER to postgres;