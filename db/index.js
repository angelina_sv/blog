const {Client} = require('pg');

async function connection(sql) {
    const client = new Client({
        user: 'postgres',
        host: 'localhost',
        password:'123',
        port: '5432',
        database:'blog_db'
    });

    await client.connect();
    const result = await client.query(sql);
    await client.end();

    return result.rows[0];
}

function writeUserData(nickname, email, password) {
    const sql = `
        INSERT INTO users (
            nickname,
            email,
            password
        )
        VALUES (
            '${nickname}',
            '${email}',
            '${password}'
        );    
    `;

    return connection(sql);
}

function authentication(email) {
    const sql = `
        SELECT id, email, password
        FROM users
        WHERE email = '${email}';
    `;

    return connection(sql);
}

async function writeArticle(id, name, text) {
    const sql = `
        INSERT INTO articles (author_id, title, text)
        VALUES (
            (
                SELECT id 
                FROM users
                WHERE id = ${id}
            ),
            '${name}',
            '${text}'
        )

        RETURNING *;
    `;

    const {text: articleText, ...data} = await connection(sql);
    return {...data, text: JSON.parse(articleText)};
}

async function getUserPublication(id) {
    const sql = `
        SELECT * 
        FROM articles
        WHERE id = ${id};
    `;

    return connection(sql);
}

async function getPublications() {
    const sql = `
        SELECT id, author_id, text, created_at 
        FROM articles 
        ORDER BY id 
        DESC LIMIT 5;
    `;

    const client = new Client({
        user: 'postgres',
        host: 'localhost',
        password:'123',
        port: '5432',
        database:'blog_db'
    });

    await client.connect();
    const result = await client.query(sql);
    await client.end();

    return result.rows;
};

async function getUserPublications(userId) {
    const sql = `
        SELECT id, author_id, text, created_at 
        FROM articles 
        WHERE author_id = ${userId}
        ORDER BY id DESC;
    `;

    const client = new Client({
        user: 'postgres',
        host: 'localhost',
        password:'123',
        port: '5432',
        database:'blog_db'
    });

    await client.connect();
    const result = await client.query(sql);
    await client.end();

    return result.rows;
}

function deletePublication(id, userId) {
    const sql = `
        DELETE FROM articles
        WHERE id = ${id} AND author_id = ${userId};
    `;

    return connection(sql);
}

function editPublication(id, userId, newValue) {
    const {name, text, picture} = newValue;
    const sql = `
        UPDATE articles
        SET text = '${text}',
            title = '${name}',
            picture_path = '${picture}'
        WHERE id = ${id} AND author_id = ${userId}
        RETURNING *;
    `;

    return connection(sql);
}

module.exports = {
    writeUserData, 
    authentication,
    writeArticle,
    getUserPublication,
    getPublications,
    getUserPublications,
    deletePublication,
    editPublication,
};