const express = require('express');
const router = express.Router();
const db = require('../../db/index.js');
const {
  hashPassword,
  generateToken
} = require('../../common/helpers/authHelper');

router.get('/register', function(req, res) {
  res.render('userRegistration');
});

router.post('/register', async function(req, res) {
  const nickname = req.body.nickname;
  const email = req.body.email;
  const password = req.body.password;
  try {
    await db.writeUserData(nickname, email, hashPassword(password));
    res.redirect('/');
  } catch(e){
    res.render('error', {message: 'Registration error', error: e});
    }
});

router.post('/login', async function(req, res) {
  try {
    const {password, ...userData} = await db.authentication(req.body.email);
    if (hashPassword(req.body.password) === password) {
      const token = await generateToken(userData);
      res.cookie('token', token, { maxAge: 3600 * 24 * 1000 });
      res.redirect('/publications');
    } else {
      res.render('error', {message: 'You entered an incorrect username or password'});
    }
  } catch(e) {
    res.render('error', {message: e});
  }

});

router.get('/logout', function(req, res) {
  res.cookie('token', '', { maxAge: 0 });
  res.clearCookie('token');
  return res.redirect('/');
});

module.exports = router;
