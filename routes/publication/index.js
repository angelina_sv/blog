const express = require('express');
const router = express.Router();
const path = require('path');

const db = require('../../db/index.js');

router.get('/add', function(req, res) {
    res.render('postPublication', { logged: true });
  });
  
  router.post('/add', async function(req, res) {
    const author_id = req.user.id;
    const name = req.body.articleName;
    const text = req.body.article;
    let pictureName;
    let picturePath;
  
    if(!req.files[0]) {
      picturePath = null;
    } else {
      pictureName = req.files[0].filename;
      picturePath = path.join(__dirname, '../..', 'public', 'images', pictureName);
    }
  
    if ((!name) || (!text)) {
        res.send('All fields must be filled');
    }
  
    const obj = {
        name: name,
        text: text,
        picture: picturePath
    } 
    const jsonText = JSON.stringify(obj);
    await db.writeArticle(author_id, name, jsonText);
    res.redirect('publications');
  });
  
  router.post('/edit/:id', async function(req, res) {
    const { id } = req.params;
    const userId = req.user.id;
    const name = req.body.name;
    const text = req.body.text;
    let pictureName;
    let picturePath;
  
    if(!req.files[0]) {
      picturePath = null;
    } else {
      pictureName = req.files[0].filename;
      picturePath = path.join(__dirname, '../..', 'public', 'images', pictureName);
    }
  
    const obj = {
      name: name,
      text: text,
      picture: picturePath
    } 
  
    try {
      await db.editPublication(id, userId, obj);
      res.status(200).send();
    } catch {
      res.status(500).send();
    }
    
  });

router.delete('/:id', async function(req, res) {
    try {
        await db.deletePublication(req.params.id, req.user.id);

        res.send('The post has been deleted');
    } catch(e) {
        return res.status(500).json({ message: 'Internal Server Error' });
    }
})

router.get('/edit/:id', async function(req, res) {
    const { id } = req.params;
    const publication = await db.getUserPublication(id);
    res.render('editPublication', { data: publication, logged: true });
})

router.get('', async function(req, res) {
    const { id } = req.user;
    const publications = await db.getUserPublications(id);
    res.render('userPublications', { data: publications, logged: true });
});

router.get('/:id', async function(req, res) {
    const { id } = req.params;
    const publication = await db.getUserPublication(id);
    res.render('userPublication', { data: publication, logged: true});
});

module.exports = router;