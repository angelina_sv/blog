const express = require('express');
const router = express.Router();

const db = require('../../db/index.js');

router.get('/', async function(req, res) {
  const publications = await db.getPublications();
  const logged = !!req.user;
  
  res.render('main', { data: publications, logged });
});

  module.exports = router;