# Prepare
    install postgres db
    npm i
    create .env file and place at root
    .env format: 
        PRIVATE_TOKEN_KEY=%generate your key%
# Prepare DB
    If you run first you should run this
        psql -U postgres -h localhost -c 'CREATE DATABASE blog_db;'
    db-migrate up
# Start
    npm start
